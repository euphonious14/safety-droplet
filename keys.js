function checkMeasures(){

  var leftNotes = MyApp.totalNotes[0];
  var rightNotes = MyApp.totalNotes[1];

  if(leftNotes!=rightNotes){
    $(".error").html("L: "+Math.round(leftNotes)+", R: "+Math.round(rightNotes));
    $(".error").show();
  }

}

function showSong(){

  presentHand("right");
  presentHand("left");

}

function lastSong(){

  if(MyApp.selectedSong==0){
    MyApp.selectedSong = MyApp.songs.length-1;
  }else{
    MyApp.selectedSong--;
  }

  $('.display').empty();
  updateSong();

}

function nextSong(){

  if(MyApp.selectedSong>=MyApp.songs.length-1){
    MyApp.selectedSong = 0;
  }else{
    MyApp.selectedSong++;
  }

  $('.display').empty();
  updateSong();

}

function changeImage(img){
  $(".image").attr("src",img+".png");
}

function toggleKeys(){
  $('body').toggleClass('stop-scrolling');
  $('.overlay').toggle();
  $('.details').toggle();
}

function calculateSongLength(){
  // songLength = [ (beats per measure x measures) / tempo ] x (60 seconds / minute)
  return ((MyApp.beatsPerMeasure * MyApp.totalMeasures) / MyApp.songs[MyApp.selectedSong].tempo)*60;
}

function updateSong(){

  MyApp.startTimer;
  MyApp.tempoInterval;
  MyApp.playing       = false;
  MyApp.showTempo     = false;
  MyApp.measure       = 0;
  MyApp.smallestNote  = smallestNote();
  MyApp.incrementNote = MyApp.smallestNote;
  // MyApp.incrementNote = 0.5;  // 1/2
  // MyApp.incrementNote = 0.25;  // 1/4
  // MyApp.incrementNote = 0.125;  // 1/8
  // MyApp.incrementNote = 0.0625; // 1/16
  if(MyApp.songs[MyApp.selectedSong].smallestNote){
    MyApp.incrementNote = slash2Decimal(MyApp.songs[MyApp.selectedSong].smallestNote);
  }
  MyApp.totalNotes    = totalNotes();
  MyApp.totalMeasures = totalMeasures();

  MyApp.beatsPerMeasure   = beatsPerMeasure();
  MyApp.beatUnit          = beatUnit();
  MyApp.squaresPerMeasure = squaresPerMeasure();
  // MyApp.smallestNoteUnit = song.smallestNote.split('/')[1];
  // MyApp.noteDecimal      = 1/MyApp.smallestNoteUnit;
  // MyApp.measureLength    = (MyApp.beatsPerMeasure/MyApp.beatUnit)*MyApp.smallestNoteUnit;

  MyApp.measureFactor = MyApp.beatsPerMeasure/MyApp.beatUnit;
  MyApp.offset 				= getOffset();

  console.log("============================");
  console.log(MyApp.songs[MyApp.selectedSong].title);
  console.log("tS  = "+MyApp.songs[MyApp.selectedSong].timeSignature);
  console.log("bPM = "+MyApp.beatsPerMeasure);
  console.log("bU  = "+MyApp.beatUnit);
  console.log("sN  = "+MyApp.smallestNote);
  console.log("iN  = "+MyApp.incrementNote);
  console.log("sPM = "+MyApp.squaresPerMeasure);
  console.log("sD  = "+squareDuration("1/2"));
  console.log("tN  = "+MyApp.totalNotes);
  console.log("tM  = "+MyApp.totalMeasures);
  console.log("mF  = "+MyApp.measureFactor);
  console.log("offset = "+MyApp.offset);
  var songIndex = MyApp.selectedSong+"/"+(MyApp.songs.length-1);
  console.log("songIndex = "+songIndex);
  $(".songIndex").html(songIndex);
  console.log("============================");

  stopPlaying();
  $("body").scrollTop();

  var song = MyApp.songs[MyApp.selectedSong];

  $(".error").hide();
  checkMeasures();
  showSong();

  $(".title").html(song.title);
  $(".desc1").html(song.key);
  $(".desc2").html(song.timeSignature);
  $(".desc3").html(song.tempo);

  var songLength = calculateSongLength();
  $(".desc4").html(Math.round(songLength)+' s');
  $(".desc5").html(decimal2Slash(MyApp.incrementNote));
  $('.md-song .length').html(Math.round(songLength)+'s');

}

function toggleTempo(){

  MyApp.showTempo=!MyApp.showTempo;
  if(MyApp.showTempo){
    $('.tempo-button').html("Toggle Tempo (On)");
  }else{
    $('.tempo-button').html("Toggle Tempo (Off)");
  }
}

function stopPlaying(){
  clearInterval(MyApp.tempoInterval);
  clearTimeout(MyApp.startTimer);
  $('.wrapper').hide();
  $('.details').css("border-bottom-color","#e3e3e3");
  $('body').clearQueue();
  $('body').stop();
  $('.start-button').html("Start");
}

function togglePlay(){

  stopPlaying();

  if(!MyApp.playing){

    $('.start-button').html("Stop");
    // $('.wrapper').show();

    // MyApp.startTimer = setTimeout(function(){
    //   $('.wrapper').hide();
    // },5000);

    $('body').stop();
    // $('body').clearQueue();

    $('body').delay(1000).animate({
      'scrollTop': $('body').height()-window.innerHeight+40
    }, (calculateSongLength()*1)*1000, "linear");

    if(MyApp.showTempo){

      var val = 0;
      MyApp.tempoInterval = setInterval(function(){
        if(val==0) {
          $('.details').css("border-bottom-color","black");
          val=1;
        }else{
          $('.details').css("border-bottom-color","#e3e3e3");
          val=0;
        }
      },(60/MyApp.songs[MyApp.selectedSong].tempo)*1000);

    }

  }

  MyApp.playing=!MyApp.playing;

}

function presentHand(hand){

  var song = MyApp.songs[MyApp.selectedSong];

  if(hand=="right"){
    handObj = song.notes[0].right;
  }else{
    handObj = song.notes[0].left;
  }

  var measure = '<div class="keys left-5">&nbsp;</div>\
  <div class="keys left-4">&nbsp;</div>\
  <div class="keys left-3">&nbsp;</div>\
  <div class="keys left-2">&nbsp;</div>\
  <div class="keys left-1">&nbsp;</div>\
  <div style="display:inline-block;width:10px;">&nbsp;</div>\
  <div class="keys right-1">&nbsp;</div>\
  <div class="keys right-2">&nbsp;</div>\
  <div class="keys right-3">&nbsp;</div>\
  <div class="keys right-4">&nbsp;</div>\
  <div class="keys right-5">&nbsp;</div>';

  var count = 0;
  var lastCounti = 0;
  var noteArray = handObj;

  for(i=0;i<noteArray.length;i++){

    // console.log("["+Math.round((i+1)/noteArray.length*100)+"%] "+hand+" ==> "+(i+1)+"/"+noteArray.length);
    var colorHex;
    var colorWord;
    var note;
    var squareText;
    var noteSet = noteArray[i];
    var combo = slash2Decimal(noteSet.duration)*MyApp.squaresPerMeasure;

    for(j=0;j<combo;j++){

      if(hand=="right"){
        $(".display").append('<div class="measure measure-'+count+'">\
        <div class="keys count count-'+count+'">&nbsp;</div>'+measure+'\
        <div class="keys count count-'+count+'">&nbsp;</div>\
        </div>');
      }

      var notes = noteSet.note;

      for(m=0;m<notes.length;m++){
        //m  is an increment through rightNotes.

        for(k=0; k<MyApp.notes.length; k++){
          // k is an increment through C-B notes.

          var squareNote = notes[m];

          if(MyApp.notes[k].note==squareNote){
            colorHex = MyApp.notes[k].colorHex;
            colorWord = MyApp.notes[k].colorWord;
            note = MyApp.notes[k].note;
            break;

          }

        }

        try{
          squareText = noteSet.text[m];
          if(lastCounti==i && i!=0){
            squareText = "";
          }
          if(squareText=="") squareText = "&nbsp;";
        }catch (err){
          squareText = "&nbsp;";
        }

        var squareFinger = noteSet.finger[m];

        // if((count/MyApp.squaresPerMeasure) % 1 == 0){
        // 	$(".count-"+count).html((count/MyApp.squaresPerMeasure)+1);
        // }

        if((count/MyApp.beatsPerMeasure) % 1 == 0){
          $(".count-"+(count+Number(MyApp.offset))).html((count/MyApp.beatsPerMeasure)+1);
        }

        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("background-color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).html(squareText);

        if(lastCounti!=i || count==0){
          if(colorHex=="#ffff00" || colorHex=="#ffd27f"){
            colorHex = "black";
          }else{
            colorHex = "#e3e3e3";
          }
        }

        if(count!=0) $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-top-color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-bottom-color",colorHex);
        if((count+1)/MyApp.squaresPerMeasure==MyApp.totalMeasures) $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-bottom-color","black");

      }

      lastCounti = i;
      count++;

    }

  }

}


function beatsPerMeasure(){
  // returns beats per measure
  // input: MyApp.songs

  var song = MyApp.songs[MyApp.selectedSong];
  return song.timeSignature.split("/")[0];

}

function beatUnit(){
  // returns beat unit
  // input: MyApp.songs

  var song = MyApp.songs[MyApp.selectedSong];
  return song.timeSignature.split("/")[1];

}

function smallestNote(){
  // defines MyApp.smallestNote
  // iterates over all notes in song
  // and finds smallest note
  // input: MyApp.songs
  // output: smallestNote

  var currentNote;
  var value = 1;
  var songNotes = MyApp.songs[MyApp.selectedSong].notes[0];

  for(j=0;j<songNotes.right.length;j++){
    currentNote = slash2Decimal(songNotes.right[j].duration);
    if(currentNote<value) value = currentNote;
  }

  for(j=0;j<songNotes.left.length;j++){
    currentNote = slash2Decimal(songNotes.left[j].duration);
    if(currentNote<value) value = currentNote;
  }

  return value;

}

function slash2Decimal(value){
  // returns decimal value
  // converts "1/2" to 0.5

  if(value.indexOf("/")>=0){
    value = value.split("/");
    return Number(value[0]/value[1]);
  }else{
    return Number(value);
  }


}

function decimal2Slash(value){
  // returns slash string
  // converts 0.5 to "1/2"

  if(String(value).indexOf(".")>=0){
    value = 1/value;
    return "1/"+value;
  }else{
    return value;
  }


}

function squaresPerMeasure(){
  // calculates squares per measure
  // input: MyApp.smallestNote

  return 1/MyApp.incrementNote;
}

function squareDuration(noteType){
  // calculates square duration based on note
  // input: noteType, MyApp.incrementNote

  return slash2Decimal(noteType)/MyApp.incrementNote;
}

function getOffset(){

  if(MyApp.songs[MyApp.selectedSong].offset){
    return Number(MyApp.songs[MyApp.selectedSong].offset);
  }else{
    return 0;
  }
}

function totalNotes(){
  // calculates leftNotes and rightNotes
  // input: MyApp.songs
  // output: [leftNotes, rightNotes]

  var songNotes = MyApp.songs[MyApp.selectedSong].notes[0];
  leftNotes  = 0;
  rightNotes = 0;

  for(j=0;j<songNotes.right.length;j++){
    rightNotes = rightNotes + slash2Decimal(songNotes.right[j].duration);
  }

  for(j=0;j<songNotes.left.length;j++){
    leftNotes = leftNotes + slash2Decimal(songNotes.left[j].duration);
  }

  return [leftNotes, rightNotes]

}

function totalMeasures(){
  // calculates total measures in song
  // input: [leftNotes, rightNotes], beatsPerMeasure()

  notes = Math.max.apply(null, MyApp.totalNotes);
  return Math.ceil(notes);

}

// MyApp.selectedSong = 1;
// MyApp.selectedSong = MyApp.songs.length-1;
//
// updateSong();

function findSongIndex(key, title){
  key   = key+" Major";
  title = title.innerHTML;

  console.log("key:   "+key);
  console.log("title: "+title);

  for(i=0;i<MyApp.songs.length;i++){
    if(MyApp.songs[i].key==key && MyApp.songs[i].title==title){
      break;
    }
  }


  if(i==MyApp.songs.length+1){
    alert('no results')
  }else{
    MyApp.selectedSong = i;
    $('.display').empty();
    $('.dropdown-content').toggle();
    updateSong();
  };

}

function selectSong(title){

  for(i=0;i<MyApp.songs.length;i++){
    if(MyApp.songs[i].title==title){
      break;
    }
  }

  if(i==MyApp.songs.length+1){
    alert('no results')
  }else{
    MyApp.selectedSong = i;
    $('.display').empty();
    $('.dropdown-content').toggle();
    updateSong();
  };


}
