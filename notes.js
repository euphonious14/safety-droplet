MyApp = {};

MyApp.notes = [
	{note: "C",	color: "red",	colorHex: "#ff0000", colorWord: "Caboose"},
	{note: "D",	color: "orange", colorHex: "#EE7600",	colorWord: "Dog"},
	{note: "E",	color: "yellow", colorHex: "#ffff00", colorWord: "Egg"},
	{note: "F",	color: "green",	colorHex: "#008000", colorWord: "Frog"},
	{note: "G",	color: "blue", colorHex: "#0000ff", colorWord: "Gem"},
	{note: "A",	color: "purple", colorHex: "#800080", colorWord: "Amethyst"},
	{note: "B",	color: "black",	colorHex: "#000000", colorWord: "Black"},
	{note: "CS", color: "Light Red", colorHex: "#ff7f7f", colorWord: "Light Caboose"},
	{note: "DS", color: "Light Orange",	colorHex: "#ffd27f", colorWord: "Light Dog"},
	{note: "FS", color: "Light Green", colorHex: "#7fbf7f", colorWord: "Light Frog"},
	{note: "GS", color: "Light Blue", colorHex: "#7f7fff", colorWord: "Light Gem"},
	{note: "AS", color: "Light Purple", colorHex: "#bf7fbf", colorWord: "Light Amethyst"},
	{note: "ES", color: "green", colorHex: "#008000", colorWord: "Frog"}
];

MyApp.note = [{
	"note": "C",
	"color": "red",
	"colorHex": "#ff0000",
	"colorWord": "Caboose"
}];

MyApp.songs = [];
