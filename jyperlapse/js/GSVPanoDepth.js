var GSVPANO = GSVPANO || {};
GSVPANO.PanoDepthLoader = function (parameters) {
    'use strict';
    var _parameters = parameters || {},
        onDepthLoad = null;
    this.load = function(panoId) {
        var self = this;
        var decoded, depthMap;
        depthMap = self.createEmptyDepthMap();
        self.depthMap = depthMap;
        self.onDepthLoad();
    }
    this.createEmptyDepthMap = function() {
        var depthMap = {
            width: 512,
            height: 256,
            depthMap: new Float32Array(512*256)
        };
        for(var i=0; i<512*256; ++i)
            depthMap.depthMap[i] = 9999999999999999999.;
        return depthMap;
    }
};
