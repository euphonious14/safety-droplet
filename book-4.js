MyApp.songs.push({
	title:"1. Walking and Running",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[
			{note:["C","","","",""], duration:"1/2", finger:["5","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/2", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/2", finger:["2","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/2", finger:["1","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/2", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/2", finger:["4","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/8", finger:["5","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/8", finger:["1","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/8", finger:["5","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/8", finger:["1","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]}
		],
		right:[
			{note:["C","","","",""], duration:"1/2", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/2", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/2", finger:["4","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/2", finger:["5","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/2", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/2", finger:["2","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/8", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/8", finger:["5","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/8", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/8", finger:["5","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/8", finger:["4","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/8", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/8", finger:["2","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]}
		]
	}]
},{
	title:"2. Skipping",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/2", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]}
		],
		right:[
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/2", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/2", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]}
		]
	}]
},{
	title:"3. Hopping",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[

		],
		right:[
			{note:["C","E","","",""], duration:"2", finger:["1","3","","",""], text:["","","","",""]},
			{note:["C","E","","",""], duration:"2", finger:["1","3","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["E","G","","",""], duration:"2", finger:["3","5","","",""], text:["","","","",""]},
			{note:["E","G","","",""], duration:"2", finger:["3","5","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["C","E","","",""], duration:"2", finger:["1","3","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["E","G","","",""], duration:"2", finger:["3","5","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["C","E","","",""], duration:"2", finger:["1","3","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["E","G","","",""], duration:"2", finger:["3","5","","",""], text:["","","","",""]},
			{note:["D","F","","",""], duration:"2", finger:["2","4","","",""], text:["","","","",""]},
			{note:["C","E","","",""], duration:"8", finger:["1","3","","",""], text:["","","","",""]}
		]
	}]
},{
	title:"4. Deep Breathing",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[

		],
		right:[
			{note:["C","E","G","",""], duration:"4", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"4", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"4", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"2", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["B","F","G","",""], duration:"2", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"8", finger:["1","3","5","",""], text:["","","","","",""]}
		]
	}]
},{
	title:"5. Deep Knee Bend",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[
			{note:["C","E","G","",""], duration:"4", finger:["5","3","1","",""], text:[">",">",">","",""]},
			{note:["C","","","",""], duration:"4", finger:["5","","","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"4", finger:["5","2","1","",""], text:["","",">",">","",""]},
			{note:["B","","","",""], duration:"4", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["5","3","1","",""], text:[">",">",">","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"2", finger:["5","2","1","",""], text:["","",">",">","",""]},
			{note:["B","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"2", finger:["5","3","1","",""], text:[">",">",">","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["","","","","",""]},
			{note:["C","","","",""], duration:"4", finger:["5","","","",""], text:["","","","","",""]}
		],
		right:[
			{note:["C","E","G","",""], duration:"8", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"8", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"4", finger:["1","3","5","",""], text:["","","","","",""]},
			{note:["B","F","G","",""], duration:"4", finger:["1","4","5","",""], text:["<","","","",""]},
			{note:["C","E","G","",""], duration:"8", finger:["1","3","5","",""], text:["","","","","",""]}
		]
	}]
},{
	title:"6. Stretching",
	book:"A Dozen A Day - Book 1",
	timeSignature:"4/4",
	tempo:"80",
	key:"C Major",
	notes:[{
		left:[
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:[">","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:[">","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]}
		],
		right:[
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:[">","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["A","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:[">","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["5","","","",""], text:[">","","","",""]},
			{note:["B","","","",""], duration:"1/4", finger:["4","","","",""], text:["","","","",""]},
			{note:["A","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1/4", finger:["3","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"1/4", finger:["2","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1/4", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:[">","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:[">","","","",""]},
			{note:["","","","",""], duration:"2", finger:["","","","",""], text:["","","","",""]}
		]
	}]
},{
	title: "7. Stretching Right Leg Up",
	book: "A Dozen A Day - Book 1",
	timeSignature: "4/4",
	tempo: "80",
	key: "C Major",
	notes:[{
		left:[

		],
		right:[
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["A","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["A","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["A","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"2", finger:["5","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:[">","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:[">","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:[">","","","",""]},
			{note:["C","C","","",""], duration:"8", finger:["1","5","","",""], text:["",">","","",""]}
		]
	}]
},{
	title: "8. Stretching Left Leg",
	book: "A Dozen A Day - Book 1",
	timeSignature: "4/4",
	tempo: "80",
	key: "C Major",
	notes:[{
		left:[
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["F","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["F","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["F","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["E","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["E","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["D","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["D","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["1","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","","","",""], duration:"2", finger:["5","","","",""], text:["<","","","",""]},
			{note:["C","C","","",""], duration:"8", finger:["5","1","","",""], text:["<","","","",""]}
		],
		right:[
			{note:["","","","",""], duration:"40", finger:["","","","",""], text:["","","","",""]},
		]
	}]
},{
	title: "9. Cartwheels",
	book: "A Dozen A Day - Book 1",
	timeSignature: "4/4",
	tempo: "80",
	key: "C Major",
	notes:[{
		left:[
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["<","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["<","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["<","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"1", finger:["5","","","",""], text:["<<","","","",""]},
			{note:["F","","","",""], duration:"1", finger:["2","","","",""], text:["<","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["<","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"1", finger:["5","","","",""], text:["<","","","",""]},
			{note:["F","","","",""], duration:"1", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["<","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["<","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["<","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"4", finger:["1","","","",""], text:[">>","","","",""]}
		],
		right:[
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"3", finger:["5","","","",""], text:[">","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["B","","","",""], duration:"1", finger:["1","","","",""], text:["<","","","",""]},
			{note:["F","","","",""], duration:"1", finger:["4","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["D","","","",""], duration:"3", finger:["5","","","",""], text:[">","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:["","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:["","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["5","","","",""], text:["","","","",""]},
			{note:["","","","",""], duration:"3", finger:["","","","",""], text:["","","","",""]},
			{note:["C","","","",""], duration:"1", finger:["1","","","",""], text:[">>","","","",""]},
			{note:["E","","","",""], duration:"1", finger:["3","","","",""], text:[">>","","","",""]},
			{note:["G","","","",""], duration:"1", finger:["5","","","",""], text:[">>","","","",""]},
			{note:["","","","",""], duration:"4", finger:["","","","",""], text:["","","","",""]}
		]
	}]
});

// ,{
// 	title: "Parallel motion in thirds or tenths",
// 	smallestNote: "",
// 	timeSignature: "4/4",
// 	tempo: "tempo",
// 	key: "C Major",
// 	notes:[{
// 		left:[
//
// 		],
// 		right:[
//
// 		]
// 	}]
// }
