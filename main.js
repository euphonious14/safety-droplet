function getNote(){

  var noteVal = Math.floor((Math.random() * MyApp.notes.length));
  var note = MyApp.notes[noteVal].note;

  while(MyApp.lastNoteVal == noteVal || note.indexOf("S") > 0){
    noteVal = Math.floor((Math.random() * MyApp.notes.length));
    note = MyApp.notes[noteVal].note;
  }

  MyApp.lastNoteVal = noteVal;

  color     = MyApp.notes[noteVal].color;
  colorHex  = MyApp.notes[noteVal].colorHex;
  colorWord = MyApp.notes[noteVal].colorWord;

  MyApp.note[0].note      = note;
  MyApp.note[0].color     = color;
  MyApp.note[0].colorHex  = colorHex;
  MyApp.note[0].colorWord = colorWord;

}

function showNote(){
  console.log(MyApp.note[0].colorHex);
  $(".training-key").css("background-color",MyApp.note[0].colorHex);

}

function updateNote(){
  showNote();
  getNote();
};

function checkMeasures(){

  var leftNotes = MyApp.totalNotes[0];
  var rightNotes = MyApp.totalNotes[1];

  if(leftNotes!=rightNotes){
    $(".error").html("L: "+Math.round(leftNotes)+", R: "+Math.round(rightNotes));
    $(".error").show();
  }

}

function showSong(){

  presentHand("right");
  presentHand("left");

}

function toggleKeys(){
  $('body').toggleClass('stop-scrolling');
  $('.overlay').toggle();
  $('.details').toggle();
}

function calculateSongLength(){
  // songLength = [ (beats per measure x measures) / tempo ] x (60 seconds / minute)
  return ((MyApp.beatsPerMeasure * MyApp.totalMeasures) / MyApp.songs[MyApp.selectedSong].tempo)*60;
}

MyApp.editMode=false;

function getNoteFromHex(hex){
  for(i=0;i<MyApp.notes.length;i++){
    if(MyApp.notes[i].colorHex==hex){
      break;
    }
  }
  if(i==13){
    return "";
  }else{
    return MyApp.notes[i].note;
  }
}

function fillOutArray(array, count){
  var newArray = [];
  if(array.length<5){
    for(k=0;k<array.length;k++){
      newArray.push(String(array[k]))
    }
    for(k=array.length;k<count;k++){
      newArray.push("");
    }
  }else{
    newArray=array;
  }
  return newArray;
}

function arraysEqual(arr1, arr2) {

    if(arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

function showObject(){

  var leftTotal = [];
  var rightTotal = [];
  var measures = $('.display').children();
  console.log("measures.length: "+measures.length);
  for(m=0;m<measures.length;m++){

    var keys = $(measures[m]).children();

    var leftArr = [];
    var rightArr = [];
    var leftFingers = [];
    var rightFingers = [];

    for(j=0;j<keys.length;j++){
      var curKey = $(keys[j]);
      var classes = curKey.attr('class');
      if(classes){
        var classesArr = classes.split(" ");
        var hand = classesArr[1];
        var hex = hexify(curKey.css("background-color"));

        if(hand.indexOf('left')>-1){
          leftArr.push(getNoteFromHex(hex))
        }else if(hand.indexOf('right')>-1){
          rightArr.push(getNoteFromHex(hex))
        }
      }

    }

    leftArr.reverse();

    for(j=0;j<leftArr.length;j++){
      if(leftArr[j]){
        leftFingers.push(String(j+1))
      }
    }

    for(j=0;j<rightArr.length;j++){
      if(rightArr[j]){
        rightFingers.push(String(j+1))
      }
    }

    leftFingers = fillOutArray(leftFingers, 5);
    rightFingers = fillOutArray(rightFingers, 5);
    // console.log("============================")
    // console.log("["+m+"] left ---------------")
    // console.log(leftFingers);
    // console.log("["+m+"] right --------------")
    // console.log(rightFingers);
    // console.log("============================")
    leftTotal.push(leftFingers);
    rightTotal.push(rightFingers);

  }

  // console.log(leftTotal);
  // console.log(rightTotal);
  var curNotes = MyApp.songs[MyApp.selectedSong].notes[0]
  // console.log(curNotes.right)

  var sudoCount = 0;
  var changesCount = 0;
  // console.log("rightTotal.length = "+rightTotal.length);
  // console.log('right.length      = '+curNotes.right.length);
  for(l=0;l<curNotes.right.length;l++){
    // console.log('========================')
    // console.log("--------------------");
    // console.log("loop through fingers");
    // console.log("--------------------");
    // console.log('========================')
    // console.log('l                 = '+l);
    // console.log('sudoCount         = '+sudoCount);
    // console.log('right.length      = '+curNotes.right.length);
    // console.log('rightTotal.length = '+rightTotal.length);
    var cur = curNotes.right[l]
    // console.log(cur);
    // console.log('squares: '+squares);
    // console.log(cur.finger);
    // console.log(cur.note);
    // console.log(rightTotal[sudoCount]);
    // console.log(cur);
    var squares = slash2Decimal(cur.duration)/MyApp.smallestNote;

    if(squares>1){
      for(n=1;n<squares;n++){
        sudoCount=sudoCount+1;
      }
    }

    if(arraysEqual(cur.finger,rightTotal[sudoCount])){
      // console.log("SAME - no change");
    }else{
      cur.finger=rightTotal[sudoCount];
      changesCount++;
    };
    sudoCount=sudoCount+1;

  }

  console.log(changesCount+" changes")
  $('.newSongText').show();
  var rst = JSON.stringify(MyApp.songs[MyApp.selectedSong]);
  rst = rst.replace(/\"title\"/g,"\ntitle");
  rst = rst.replace(/\"book\"/g,"\nbook");
  rst = rst.replace(/\"timeSignature\"/g,"\ntimeSignature");
  rst = rst.replace(/\"tempo\"/g,"\ntempo");
  rst = rst.replace(/\"key\"/g,"\nkey");
  rst = rst.replace(/\"youtube\"/g,"\nyoutube");
  rst = rst.replace(/\"notes\"/g,"\nnotes");
  rst = rst.replace(/\"left\"/g,"\nleft");
  rst = rst.replace(/\"right\"/g,"\nright");
  rst = rst.replace(/\"note\"/g,"\nnote");
  rst = rst.replace(/\"duration\"/g,"\nduration")
  rst = rst.replace(/\"finger\"/g,"\nfinger")
  rst = rst.replace(/\"text\"/g,"\ntext")
  rst = rst.replace(/\},\{/g,"\n\},\{")
  $('textarea.newSongObj').val(rst);
  $('textarea.newSongObj').select();

}

function clickEdit(){
  console.log('==> clickEdit');
  MyApp.editMode=!MyApp.editMode;

  if(MyApp.editMode){
    $('.editTools').show();
    $('.keys').addClass("clickable");

    $('.keys').click(function(){
      console.log("---key clicked---")
      var rawColor = $(this).css("background-color")
      var keyColor = hexify(rawColor);

      var measureArray = $(this).parent().attr("class").split(" ");
      var curMeasure = measureArray[measureArray.length-1]

      if(keyColor=="#ffffff"){

        if(MyApp.lastMeasure==curMeasure){
          console.log("same measure")
          console.log("empty key");

          console.log("last key");
          console.log(MyApp.lastKey.html());

          var lastKeyColor = hexify(MyApp.lastKey.css("background-color"));
          $(this).css("background-color",lastKeyColor);
          $(this).css("border-top-color",MyApp.lastKey.css("border-top-color"));
          $(this).css("border-bottom-color",MyApp.lastKey.css("border-bottom-color"));
          $(this).css("color",MyApp.lastKey.css("color"));
          $(this).html(MyApp.lastKey.html());
          MyApp.lastKey.css("background-color",rawColor)
          MyApp.lastKey.css("border-top-color","black")
          MyApp.lastKey.css("border-bottom-color","black")
          MyApp.lastKey.html("&nbsp;");
          $('.editBox1').css("background-color",rawColor);
          $('.editBox1').html("&nbsp;");
          $('.editBox2').css("background-color",lastKeyColor);
        }

      }else{
        console.log("colored key");
        MyApp.lastKey = $(this);
        MyApp.lastMeasure = curMeasure;
        $('.editBox1').css("background-color",hexify(MyApp.lastKey.css("background-color")));
        $('.editBox1').css("color",MyApp.lastKey.css("color"));
        $('.editBox1').html(MyApp.lastKey.html());
        $('.editBox2').css("background-color","rgba(0,0,0,0)");
      }

    })
  }else{
    $('.editTools').hide();
    $('.keys').off("click");
    $('.keys').removeClass("clickable");
  }





}

function updateSong(){

  MyApp.startTimer;
  MyApp.tempoInterval;
  MyApp.playing       = false;
  MyApp.showTempo     = false;
  MyApp.measure       = 0;
  MyApp.smallestNote  = smallestNote();
  MyApp.incrementNote = MyApp.smallestNote;
  // MyApp.incrementNote = 0.5;  // 1/2
  // MyApp.incrementNote = 0.25;  // 1/4
  // MyApp.incrementNote = 0.125;  // 1/8
  // MyApp.incrementNote = 0.0625; // 1/16
  if(MyApp.songs[MyApp.selectedSong].smallestNote){
    MyApp.incrementNote = slash2Decimal(MyApp.songs[MyApp.selectedSong].smallestNote);
  }
  MyApp.totalNotes    = totalNotes();
  MyApp.totalMeasures = totalMeasures();

  MyApp.beatsPerMeasure   = beatsPerMeasure();
  MyApp.beatUnit          = beatUnit();
  MyApp.squaresPerMeasure = squaresPerMeasure();
  // MyApp.smallestNoteUnit = song.smallestNote.split('/')[1];
  // MyApp.noteDecimal      = 1/MyApp.smallestNoteUnit;
  // MyApp.measureLength    = (MyApp.beatsPerMeasure/MyApp.beatUnit)*MyApp.smallestNoteUnit;

  MyApp.measureFactor = MyApp.beatsPerMeasure/MyApp.beatUnit;
  MyApp.offset 				= getOffset();

  console.log("============================");
  console.log(MyApp.songs[MyApp.selectedSong].title);
  console.log("tS  = "+MyApp.songs[MyApp.selectedSong].timeSignature);
  console.log("bPM = "+MyApp.beatsPerMeasure);
  console.log("bU  = "+MyApp.beatUnit);
  console.log("sN  = "+MyApp.smallestNote);
  console.log("iN  = "+MyApp.incrementNote);
  console.log("sPM = "+MyApp.squaresPerMeasure);
  console.log("sD  = "+squareDuration("1/2"));
  console.log("tN  = "+MyApp.totalNotes);
  console.log("tM  = "+MyApp.totalMeasures);
  console.log("mF  = "+MyApp.measureFactor);
  console.log("offset = "+MyApp.offset);
  var songIndex = MyApp.selectedSong+"/"+(MyApp.songs.length-1);
  console.log("songIndex = "+songIndex);
  $(".songIndex").html(songIndex);
  console.log("============================");

  stopPlaying();
  $("body").scrollTop(0);

  var song = MyApp.songs[MyApp.selectedSong];

  $(".error").hide();
  checkMeasures();
  showSong();

  $('.md-song .song').html(song.title);
  $('.md-song .book').html(song.book);
  $('.md-song .key').html(song.key);
  $('.md-song .tempo').html(song.tempo);
  if(song.youtube){
    $('#video').attr('src',song.youtube)
    $('.player').show();
  }else{
    $('#video').attr('src','')
    $('.player').hide();
  }


  var songLength = calculateSongLength();
  $(".desc4").html(Math.round(songLength)+' s');
  $(".desc5").html(decimal2Slash(MyApp.incrementNote));
  $('.md-song .length').html(Math.round(songLength)+'s');

}

function toggleTempo(){
  MyApp.showTempo=!MyApp.showTempo;
  if(MyApp.showTempo){
    $('.tempo-button').html("Toggle Tempo (On)");
  }else{
    $('.tempo-button').html("Toggle Tempo (Off)");
  }
}

function stopPlaying(){
  clearInterval(MyApp.tempoInterval);
  clearTimeout(MyApp.startTimer);
  $('.wrapper').hide();
  $('.details').css("border-bottom-color","#e3e3e3");
  $('body').clearQueue();
  $('body').stop();
  $('.start-button').html("Start");
}

function togglePlay(){

  stopPlaying();

  if(!MyApp.playing){

    $('.start-button').html("Stop");
    // $('.wrapper').show();

    // MyApp.startTimer = setTimeout(function(){
    //   $('.wrapper').hide();
    // },5000);

    $('body').stop();
    // $('body').clearQueue();

    $('body').delay(3000).animate({
      'scrollTop': $('body').height()-window.innerHeight+40
    }, (calculateSongLength()*1)*1000, "linear");

    if(MyApp.showTempo){

      var val = 0;
      MyApp.tempoInterval = setInterval(function(){
        if(val==0) {
          $('.details').css("border-bottom-color","black");
          val=1;
        }else{
          $('.details').css("border-bottom-color","#e3e3e3");
          val=0;
        }
      },(60/MyApp.songs[MyApp.selectedSong].tempo)*1000);

    }

  }

  MyApp.playing=!MyApp.playing;

}

function presentHand(hand){

  var song = MyApp.songs[MyApp.selectedSong];

  if(hand=="right"){
    handObj = song.notes[0].right;
  }else{
    handObj = song.notes[0].left;
  }

  var measure = '<div class="keys left-5">&nbsp;</div>\
  <div class="keys left-4">&nbsp;</div>\
  <div class="keys left-3">&nbsp;</div>\
  <div class="keys left-2">&nbsp;</div>\
  <div class="keys left-1">&nbsp;</div>\
  <div style="display:inline-block;width:10px;">&nbsp;</div>\
  <div class="keys right-1">&nbsp;</div>\
  <div class="keys right-2">&nbsp;</div>\
  <div class="keys right-3">&nbsp;</div>\
  <div class="keys right-4">&nbsp;</div>\
  <div class="keys right-5">&nbsp;</div>';

  var count = 0;
  var lastCounti = 0;
  var noteArray = handObj;

  for(i=0;i<noteArray.length;i++){

    // console.log("["+Math.round((i+1)/noteArray.length*100)+"%] "+hand+" ==> "+(i+1)+"/"+noteArray.length);
    var colorHex;
    var colorWord;
    var note;
    var squareText;
    var noteSet = noteArray[i];
    var combo = slash2Decimal(noteSet.duration)*MyApp.squaresPerMeasure;

    for(j=0;j<combo;j++){

      if(hand=="right"){
        $(".display").append('<div class="measure measure-'+count+'">\
        <div class="keys count count-'+count+'">&nbsp;</div>'+measure+'\
        <div class="keys count count-'+count+'">&nbsp;</div>\
        </div>');
      }

      var notes = noteSet.note;

      for(m=0;m<notes.length;m++){
        //m  is an increment through rightNotes.

        for(k=0; k<MyApp.notes.length; k++){
          // k is an increment through C-B notes.

          var squareNote = notes[m];

          if(MyApp.notes[k].note==squareNote){
            colorHex = MyApp.notes[k].colorHex;
            colorWord = MyApp.notes[k].colorWord;
            note = MyApp.notes[k].note;
            break;

          }

        }

        try{
          squareText = noteSet.text[m];
          if(lastCounti==i && i!=0){
            squareText = "";
          }
          if(squareText=="") squareText = "&nbsp;";
        }catch (err){
          squareText = "&nbsp;";
        }

        var squareFinger = noteSet.finger[m];

        // if((count/MyApp.squaresPerMeasure) % 1 == 0){
        // 	$(".count-"+count).html((count/MyApp.squaresPerMeasure)+1);
        // }

        if((count/MyApp.beatsPerMeasure) % 1 == 0){
          $(".count-"+(count+Number(MyApp.offset))).html((count/MyApp.beatsPerMeasure)+1);
        }

        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("background-color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).html(squareText);

        if(lastCounti!=i || count==0){
          if(colorHex=="#ffff00" || colorHex=="#ffd27f"){
            colorHex = "black";
          }else{
            colorHex = "#e3e3e3";
          }
        }

        if(count!=0) $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-top-color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("color",colorHex);
        $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-bottom-color",colorHex);
        if((count+1)/MyApp.squaresPerMeasure==MyApp.totalMeasures) $(".measure-"+count+" > ."+hand+"-"+squareFinger).css("border-bottom-color","black");

      }

      lastCounti = i;
      count++;

    }

  }

}


function beatsPerMeasure(){
  // returns beats per measure
  // input: MyApp.songs

  var song = MyApp.songs[MyApp.selectedSong];
  return song.timeSignature.split("/")[0];

}

function beatUnit(){
  // returns beat unit
  // input: MyApp.songs

  var song = MyApp.songs[MyApp.selectedSong];
  return song.timeSignature.split("/")[1];

}

function smallestNote(){
  // defines MyApp.smallestNote
  // iterates over all notes in song
  // and finds smallest note
  // input: MyApp.songs
  // output: smallestNote

  var currentNote;
  var value = 1;
  var songNotes = MyApp.songs[MyApp.selectedSong].notes[0];

  for(j=0;j<songNotes.right.length;j++){
    currentNote = slash2Decimal(songNotes.right[j].duration);
    if(currentNote<value) value = currentNote;
  }

  for(j=0;j<songNotes.left.length;j++){
    currentNote = slash2Decimal(songNotes.left[j].duration);
    if(currentNote<value) value = currentNote;
  }

  return value;

}

function slash2Decimal(value){
  // returns decimal value
  // converts "1/2" to 0.5

  if(value.indexOf("/")>=0){
    value = value.split("/");
    return Number(value[0]/value[1]);
  }else{
    return Number(value);
  }


}

function decimal2Slash(value){
  // returns slash string
  // converts 0.5 to "1/2"

  if(String(value).indexOf(".")>=0){
    value = 1/value;
    return "1/"+value;
  }else{
    return value;
  }


}

function squaresPerMeasure(){
  // calculates squares per measure
  // input: MyApp.smallestNote

  return 1/MyApp.incrementNote;
}

function squareDuration(noteType){
  // calculates square duration based on note
  // input: noteType, MyApp.incrementNote

  return slash2Decimal(noteType)/MyApp.incrementNote;
}

function getOffset(){

  if(MyApp.songs[MyApp.selectedSong].offset){
    return Number(MyApp.songs[MyApp.selectedSong].offset);
  }else{
    return 0;
  }
}

function totalNotes(){
  // calculates leftNotes and rightNotes
  // input: MyApp.songs
  // output: [leftNotes, rightNotes]

  var songNotes = MyApp.songs[MyApp.selectedSong].notes[0];
  leftNotes  = 0;
  rightNotes = 0;

  for(j=0;j<songNotes.right.length;j++){
    rightNotes = rightNotes + slash2Decimal(songNotes.right[j].duration);
  }

  for(j=0;j<songNotes.left.length;j++){
    leftNotes = leftNotes + slash2Decimal(songNotes.left[j].duration);
  }

  return [leftNotes, rightNotes]

}

function totalMeasures(){
  // calculates total measures in song
  // input: [leftNotes, rightNotes], beatsPerMeasure()

  notes = Math.max.apply(null, MyApp.totalNotes);
  return Math.ceil(notes);

}

function findSongIndex(key, title){
  key   = key+" Major";
  title = title.innerHTML;

  console.log("key:   "+key);
  console.log("title: "+title);

  for(i=0;i<MyApp.songs.length;i++){
    if(MyApp.songs[i].key==key && MyApp.songs[i].title==title){
      break;
    }
  }


  if(i==MyApp.songs.length+1){
    alert('no results')
  }else{
    MyApp.selectedSong = i;
    $('.display').empty();
    $('.dropdown-content').toggle();
    updateSong();
  };

}

function clickAbout(){
  $('.results').empty();
  $('.option').css('color','white');
  console.log('click about')
  $('.results').html('<div style="padding-top:30px;line-height:25px;margin:0 auto;width:365px;text-align:left;">\
                      <span style="color:#00ACC1">Klavair</span> is a tool meant to help one conquer his or her musical pursuits.\
                      It\'s to be used as an accompaniment or alternative to traditional sheet music. It is recommended to first get comfortable with the colors.\
                      If you are without a piano, here are great options:<br>\
                      <ul>\
                        <li>\
                          <a target="_blank" href="https://www.amazon.com/Yamaha-PSRE253-61-Key-Portable-Keyboard/dp/B00UN8O5VO">\
                            Yamaha PSRE253 61-Key Portable Keyboard (~$110)\
                          </a>\
                        </li>\
                        <li>\
                          <a target="_blank" href="https://www.amazon.com/Casio-CTK2400-61--Portable-Keyboard/dp/B00JZEW3KC">\
                            Casio CTK2400 61- Key Portable Keyboard (~$100)\
                          </a>\
                        </li>\
                      </ul>\
                      Once you\'re in possession of a piano, it\'s recommended to color removable labels and stick them to your instrument.\
                      These labels and sharpies can be used to accomplish this:\
                      <ul>\
                        <li>\
                          <a target="_blank" href="https://www.amazon.com/Avery-Removable-Labels-Rectangular-Inches/dp/B004INKGYA">\
                            Avery Removable Labels, Rectangular, 0.5 x 0.75 Inches, White, Pack of 525 (~$4)\
                          </a>\
                        </li>\
                        <li>\
                          <a target="_blank" href="https://www.amazon.com/Sharpie-Permanent-Markers-Assorted-75846/dp/B000GOZYRO">\
                            Sharpie Permanent Markers, Fine Point, Assorted Colors, 24-Pack (~$10)\
                          </a>\
                        </li>\
                      </ul>\
                      When it comes to code, the tools below have been immensely helpful in realizing <span style="color:#00ACC1">Klavair</span>:\
                      <ul>\
                        <li>\
                          <a target="_blank" href="https://abcjs.net/">abcjs</a>\
                        </li>\
                        <li>\
                          <a target="_blank" href="https://regex101.com/">regex101</a>\
                        </li>\
                      </ul>');

  $('.display').show();
}

function selectSong(title){

  for(i=0;i<MyApp.songs.length;i++){
    if(MyApp.songs[i].title==title){
      break;
    }
  }

  if(i==MyApp.songs.length+1){
    alert('no results')
  }else{
    MyApp.selectedSong = i;
    $('.display').empty();
    $('.dropdown-content').toggle();
    updateSong();
  };

}

// NEW STUFF
function showBookOptions(){
  var books = [];
  for (i=0;i<MyApp.songs.length;i++){
    if(books.indexOf(MyApp.songs[i].book)==-1){
      books.push(MyApp.songs[i].book);
      $('.options').append('<div class="option">'+MyApp.songs[i].book+'</div>');
    }
  }
}

showBookOptions();
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  console.log('mobile device');
  $('.createSong').hide();
}


function nextSong(){
  var i = MyApp.selectedSong+1;
  if(i>MyApp.songs.length-1){
    MyApp.selectedSong=0;
  }else{
    MyApp.selectedSong = i;
  }
  $('.display').empty();
  $('.dropdown-content').toggle();
  updateSong();
}

function prevSong(){
  var i = MyApp.selectedSong-1;
  if(i<0){
    MyApp.selectedSong = MyApp.songs.length-1;
  }else{
    MyApp.selectedSong = i;
  }
  $('.display').empty();
  $('.dropdown-content').toggle();
  updateSong();
}

function updateResults(book){

  for(i=0;i<MyApp.songs.length;i++){

    if(book){
      book = book.replace(/&amp;/g, '&');
      if(book==MyApp.songs[i].book){
        $('.results').append('<div class="result">'+
          '<div class="info">'+
            '<div class="title-info">'+MyApp.songs[i].title+'</div>'+
            '<div class="subtext">'+
              '<table>'+
                '<tr><td>Book</td><td class="book">'+MyApp.songs[i].book+'</td></tr>'+
                '<tr><td>Key</td><td class="key">'+MyApp.songs[i].key+'</td></tr>'+
                '<tr><td>Tempo</td><td class="tempo">'+MyApp.songs[i].tempo+'</td></tr>'+
              '</table>'+
            '</div>'+
          '</div>'+
        '</div>');
      }
    }else{
      $('.results').append('<div class="result">'+
        '<div class="info">'+
          '<div class="title-info">'+MyApp.songs[i].title+'</div>'+
          '<div class="subtext">'+
            '<table>'+
              '<tr><td>Book</td><td class="book">'+MyApp.songs[i].book+'</td></tr>'+
              '<tr><td>Key</td><td class="key">'+MyApp.songs[i].key+'</td></tr>'+
              '<tr><td>Tempo</td><td class="tempo">'+MyApp.songs[i].tempo+'</td></tr>'+
            '</table>'+
          '</div>'+
        '</div>'+
      '</div>');
    }

  }
}

updateResults();
clickResult();

// click an OPTION in SUBHEADER
$('.option').click(function(){

  $('.option').css('color','white');
  $(this).css('color','black');
  $('.results').empty();
  updateResults(this.innerHTML);
  clickResult();

});

// click a RESULT in MAIN
function clickResult(){

  $('.result').click(function(){
    $("body").scrollTop(0);
    var title = $(this).find(".title-info")[0].innerHTML;
    var book  = $(this).find(".book")[0].innerHTML;
    $('.results').hide();
    $('.options').hide();

    if(title=="Getting Started - Part 1"){

      $('.md-training .subject').html(title);
      $('.md-training .book').html(book);
      $('.md-training').show();
      $('.display').html('<img src="piano.svg"/><br><br>\
                          <div style="line-height:25px;margin:0 auto;width:365px;text-align:left;">\
                          A piano is made up of white and black keys\
                          <br>\
                          A solid color indicates a natural note (white key)\
                          </div>\
                          <br><div class="legend legend-one"></div>');

      $('.legend-one').append('<table style="margin:0 auto;">');


      for(i=0;i<7;i++){
        $('.legend-one').append('<tr style="color:'+MyApp.notes[i].colorHex+'">\
                  <td>'+MyApp.notes[i].colorWord[0]+'&nbsp;</td>\
                  <td> &#8658; '+MyApp.notes[i].colorWord+'</td>\
                </tr>');
        // $('.legend').append("<div style='padding:3px;color:"+MyApp.notes[i].colorHex+";'>"+MyApp.notes[i].colorWord[0]+" &#8658; "+MyApp.notes[i].colorWord+"</div>");
      }

      $('.legend-one').append('</table>');

      $('.display').append('<br><div style="line-height:25px;margin:0 auto;width:365px;text-align:left;">\
                            A light color indicates a sharp/flat note (black key)</div>\
                            <br><div class="legend legend-two"></div>');

      $('.legend-two').append('<table style="margin:0 auto;">');

      var lightArr = ['C&#x266F;/D&#x266D;','D&#x266F;/E&#x266D;','F&#x266F;/G&#x266D;','G&#x266F;/A&#x266D;','A&#x266F;/B&#x266D;'];
      for(i=7;i<12;i++){
        $('.legend-two').append('<tr style="color:'+MyApp.notes[i].colorHex+'">\
                  <td>'+lightArr[i-7]+'&#8658;</td>\
                  <td>&nbsp;'+MyApp.notes[i].color+'</td>\
                </tr>');
      }

      $('.legend-two').append('</table>');

    }else if(title=="Getting Started - Part 2"){

      $('.md-training .subject').html(title);
      $('.md-training .book').html(book);
      $('.md-training').show();
      $('.display').html('Squares are split into Left Hand (LH) & Right Hand (RH)<br><br>');

      var measureFirst = '<div class="keys left-5">5</div>\
        <div class="keys left-4">4</div>\
        <div class="keys left-3">3</div>\
        <div class="keys left-2">2</div>\
        <div class="keys left-1">1</div>\
        <div style="display:inline-block;width:10px;">&nbsp;</div>\
        <div class="keys right-1">&nbsp;</div>\
        <div class="keys right-2">&nbsp;</div>\
        <div class="keys right-3">&nbsp;</div>\
        <div class="keys right-4">&nbsp;</div>\
        <div class="keys right-5">&nbsp;</div>';

      $(".display").append('<div class="measure measure-0">\
        <div class="keys count count-0">LH</div>'+measureFirst+'\
        <div class="keys count count-0"></div>\
        </div>');

      var measureSecond = '<div class="keys left-5">&nbsp;</div>\
        <div class="keys left-4">&nbsp;</div>\
        <div class="keys left-3">&nbsp;</div>\
        <div class="keys left-2">&nbsp;</div>\
        <div class="keys left-1">&nbsp;</div>\
        <div style="display:inline-block;width:10px;">&nbsp;</div>\
        <div class="keys right-1">1</div>\
        <div class="keys right-2">2</div>\
        <div class="keys right-3">3</div>\
        <div class="keys right-4">4</div>\
        <div class="keys right-5">5</div>';

      $(".display").append('<div class="measure measure-0">\
        <div class="keys count count-0"></div>'+measureSecond+'\
        <div class="keys count count-0">RH</div>\
        </div><br>');

      $('.display').append('<table style="margin:0 auto;">\
        <tr>\
          <td>1 &#8658;</td>\
          <td>thumb</td>\
        </tr>\
        <tr>\
          <td>2 &#8658;</td>\
          <td>index finger</td>\
        </tr>\
        <tr>\
          <td>3 &#8658;</td>\
          <td>middle finger</td>\
        </tr>\
        <tr>\
          <td>4 &#8658;</td>\
          <td>ring finger</td>\
        </tr>\
        <tr>\
          <td>5 &#8658;</td>\
          <td>pinky finger</td>\
        </tr>\
      </table>');

      $(".keys").css("color","black");

    }else if(title=="Note Training"){

      $('.md-training .subject').html(title);
      $('.md-training .book').html(book);
      $('.md-training').show();
      $('.display').html('Say the note out loud<br><br><div class="training-keys"><div class="training-key"></div></div>');

      updateNote();
      MyApp.noteTrainingInterval = setInterval(function(){ updateNote(); }, 1500);

    }else{

      var key   = $(this).find(".key")[0].innerHTML;
      var tempo = $(this).find(".tempo")[0].innerHTML;
      selectSong(title);
      $('.md-song').show();
      $('.start-button').show();
      $('.edit-button').show();

      $('.next-button').show();
      $('.prev-button').show();

    }

    $('.footer').hide();
    $('.display').show();

  });
}

// click LOGO in HEADER
$('.logo').click(function(){

  $("body").scrollTop(0);
  $('.option').css('color','white');
  $('.metadatas').hide();
  $('.start-button').hide();
  $('.edit-button').hide();
  $('.editTools').hide();

  $('.next-button').hide();
  $('.prev-button').hide();
  $('.player').hide();
  $('#video').attr("src","")

  $('.footer').show();
  $('.display').empty();
  clearInterval(MyApp.noteTrainingInterval);
  $('.options').show();
  $('.results').show();
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    console.log('mobile device');
  }else{
    $('.createSong').show();
  }

  $('.results').empty();
  $('.results').append('<div class="result">\
            <div class="info">\
              <div class="title-info">Getting Started - Part 1</div>\
              <div class="subtext">\
                <table>\
                  <tr><td>&#8227;</td><td class="book">What the colors mean</td></tr>\
                </table>\
              </div>\
            </div>\
          </div>\
          <div class="result">\
            <div class="info">\
              <div class="title-info">Getting Started - Part 2</div>\
              <div class="subtext">\
                <table>\
                  <tr><td>&#8227;</td><td class="book">What the squares mean</td></tr>\
                </table>\
              </div>\
            </div>\
          </div>\
          <div class="result">\
            <div class="info">\
              <div class="title-info">Note Training</div>\
              <div class="subtext">\
                <table>\
                  <tr><td>&#8227;</td><td class="book">Practice recognizing notes</td></tr>\
                </table>\
              </div>\
            </div>\
          </div>');
  updateResults();
  clickResult();

});

function toggleVideo(){
  if($('.video-button').html()=='Close'){
    $('#video').hide()
    $('.video-button').html('Listen')
  }else{
    $('#video').show()
    $('.video-button').html('Close')
  }

}

function hexify(color) {
  var values = color
    .replace(/rgba?\(/, '')
    .replace(/\)/, '')
    .replace(/[\s+]/g, '')
    .split(',');
  var a = parseFloat(values[3] || 1),
      r = Math.floor(a * parseInt(values[0]) + (1 - a) * 255),
      g = Math.floor(a * parseInt(values[1]) + (1 - a) * 255),
      b = Math.floor(a * parseInt(values[2]) + (1 - a) * 255);
  return "#" +
    ("0" + r.toString(16)).slice(-2) +
    ("0" + g.toString(16)).slice(-2) +
    ("0" + b.toString(16)).slice(-2);
}
